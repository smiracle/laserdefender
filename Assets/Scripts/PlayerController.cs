﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	//quaternion.identity means no rotation			
	private float speed = 15.0f;
	public float padding = .1f;
	public GameObject projectile;
	public float projectileSpeed;
	public float firingRate = 0.2f;
	public float health = 250f;
	public AudioClip fireSound;
	
	private float xmin;
	private float xmax;
	
	void Start()
	{
		float distance = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, distance));
		Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1f, 0f, distance));
		xmin = leftmost.x + padding;
		xmax = rightmost.x - padding;
	}
	
	void Die()
	{
		LevelManager man = GameObject.Find ("LevelManager").GetComponent<LevelManager>();
		man.LoadLevel("Win Screen");
		Destroy(gameObject);
	}
	
	void Fire()
	{
		Vector3 offset = new Vector3(0f,0.5f,0f);
		GameObject beam = Instantiate (projectile, transform.position+offset, Quaternion.identity) as GameObject;
		beam.GetComponent<Rigidbody2D>().velocity = new Vector3(0,projectileSpeed,0);
		AudioSource.PlayClipAtPoint (fireSound, transform.position);
	}
	
	void Update() 
	{		
		if(Input.GetKeyDown (KeyCode.Space))
		{
			InvokeRepeating ("Fire", 0.000001f, firingRate);		
		}					
		if(Input.GetKeyUp (KeyCode.Space))
		{
			CancelInvoke("Fire");
		}					
		if (Input.GetKey(KeyCode.LeftArrow))
		{			
			//transform.position += new Vector3(-speed*Time.deltaTime, 0f, 0f);
			transform.position += Vector3.left *speed*Time.deltaTime;
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			//transform.position += new Vector3(speed*Time.deltaTime,0f,0f);
			transform.position += Vector3.right *speed*Time.deltaTime;
		}	
		float newX = Mathf.Clamp (GetComponent<Transform>().position.x, xmin, xmax);	
		//Restrict the player to the viewport area:
		GetComponent<Transform>().position = new Vector3(newX, GetComponent<Transform>().position.y, GetComponent<Transform>().position.z);
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{		
		Projectile missile = collider.gameObject.GetComponent<Projectile>();
		if(missile)
		{
			health -= missile.GetDamage();
			missile.Hit();
			if(health<=0)
			{
				Die();
			}			
		}
	}
}
