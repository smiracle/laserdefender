﻿using UnityEngine;
using System.Collections;

public class EnemyBehavior : MonoBehaviour 
{
	public GameObject projectile;
	public float health = 150f;
	public float projectileSpeed = 10f;
	public float shotsPerSecond = 0.5f;
	public int scoreValue = 150;
	private ScoreKeeper scoreKeeper;
	
	public AudioClip fireSound;
	public AudioClip deathSound;
	
	void Start()
	{
		scoreKeeper = GameObject.Find("Score").GetComponent<ScoreKeeper>();
	}
	
	void Update()
	{
		float probability = Time.deltaTime * shotsPerSecond;
		if(Random.value <probability)
		{
			Fire();
		}		
	}
	
	void Fire()
	{
		//Vector3 firePos = transform.position //+ new Vector3(0,-1f,0);
		GameObject laser = Instantiate (projectile, transform.position, Quaternion.identity) as GameObject;
		laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -projectileSpeed);
		AudioSource.PlayClipAtPoint(fireSound,transform.position);
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{		
		Projectile missile = collider.gameObject.GetComponent<Projectile>();
		if(missile)
		{
			health -= missile.GetDamage();			
			missile.Hit();
			if(health<=0)
			{
				Die ();
			}			
		}
	}
	
	void Die()
	{
		AudioSource.PlayClipAtPoint(deathSound, transform.position);
		scoreKeeper.Score (scoreValue);
		Destroy(gameObject);
	}
}
